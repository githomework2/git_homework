// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Git_HomeworkGameMode.generated.h"

UCLASS(minimalapi)
class AGit_HomeworkGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGit_HomeworkGameMode();
};




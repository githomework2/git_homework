// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Git_HomeworkGameMode.h"
#include "Git_HomeworkHUD.h"
#include "Git_HomeworkCharacter.h"
#include "UObject/ConstructorHelpers.h"

AGit_HomeworkGameMode::AGit_HomeworkGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AGit_HomeworkHUD::StaticClass();
}

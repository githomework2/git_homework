// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Git_HomeworkHUD.generated.h"

UCLASS()
class AGit_HomeworkHUD : public AHUD
{
	GENERATED_BODY()

public:
	AGit_HomeworkHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

